<?php
       
class Users_controller extends Controller {

	function __construct() {
		parent::__construct();
	}
        
        /**
         * 
         * @param type $id
         */
        public function getUsers($id = null){
            if(isset($id) && !is_null($id)){
                Penelope::printJSON(Usuario::getById($id));
            }else{
                Penelope::printJSON(Usuario::getAll());
            }
        }
        
        /**
         * 
         */
        public function postUsers(){
            
            $keys = Usuario::getKeys();
            $this->validateKeys($keys, filter_input_array(INPUT_POST));

            $_POST["password"] = Hash::create(filter_input(INPUT_POST, "password"));
            $usr = Usuario::instanciate($_POST);
            $r = $usr->create();
            
            if($r["error"] == 0){
                $msg = $r["msg"];
                $args = ["user"=>  Penelope::arrayToJSON($usr->toArray())];
            }else{
                $msg = $r["msg"];
                $args = [];
            }
            
            $response = Request::response($msg, $args, $r["error"]);
            Penelope::printJSON($response);
        }
        
        /**
         * 
         */
        public function putUsers(){
            $_PUT = $this->_PUT;
            $usr = Usuario::getById($_PUT["id"]);

            foreach ($_PUT as $key => $value) {
                if($key == "password"){
                    $password = Hash::create(filter_input(INPUT_POST, "password"));
                    $usr->{"set".ucfirst($key)}($password);
                }else{
                    $usr->{"set".ucfirst($key)}($value);
                }
            }
            $r = $usr->update();
            
            if($r["error"] == 0){
                $msg = $r["msg"];
                $args = ["user"=>  Penelope::arrayToJSON($usr->toArray())];
            }else{
                $msg = $r["msg"];
                $args = [];
            }
            
            $response = Request::response($msg, $args, $r["error"]);
            Penelope::printJSON($response);
        }
        
        /**
         * 
         */
        public function deleteUsers(){
            $_DELETE = $this->_DELETE;
            $usr = Usuario::getById($_DELETE["id"]);
            if($usr->getId() != NULL){
                $r = $usr->delete();
                if($r["error"] == 0){
                    $msg = $r["msg"];
                    $args = ["user"=>  Penelope::arrayToJSON($usr->toArray())];
                }else{
                    $msg = $r["msg"];
                    $args = [];
                }
            }else{
                $r["error"]=1;
                $msg = "Invalid User to Delete";
                $args = [];
            }
            $response = Request::response($msg, $args, $r["error"]);
            Penelope::printJSON($response);
            
        }
        private function secretKey($username){

            $usr = Usuario::getBy("username",$username);
            if(!is_null($usr->getId())){
                return Hash::getSecret($usr);
            }
        }
        
        private function pin($username){

            return substr($this->secretKey($username),0,4);
            
        }
        
        public function getCheckUser($username,$password){
            $user = Usuario::getBy("username", $username);
            $pPass = Hash::create($password);
            if( $pPass == $user->getPassword()){
                $msg = "Usuario válido";
                $args = ["status"=> true];
            }else{
                $msg = "Usuario invalido";
                $args = ["status"=> false];
            }
            $response = Request::response($msg, $args, 0);
            Penelope::printJSON($response);
        }
        
        public function getNewPassword($correo){
            if(!$this->exists('correo',$correo)){
                $error = 1;
                $msg = "Usuario no existe";
            }else{
                $user = Usuario::getBy("correo", $correo);
                $url = $this->pin($user->getUsername());
                $email = Hermes::getPasswordBody($user->getUsername(), $url);
                Hermes::send($user->getCorreo(), "Cambio de contraseña", 
                        "no-reply@jogoapp.com",$email);
                $error = 0;
                $msg = "Correo enviado. PIN:".$url;
            }
            $response = Request::response($msg,[], $error);
            Penelope::printJSON($response);
        }
        
        public function getValidatePin($correo,$pin){
            $user = Usuario::getBy("correo", $correo);
            if( $this->pin($user->getUsername()) == $pin){
                $msg = "Usuario válido";
                $args = ["status"=> true];
            }else{
                $msg = "Usuario invalido";
                $args = ["status"=> false];
            }
            $response = Request::response($msg, $args, 0);
            Penelope::printJSON($response);
        }
        
}
